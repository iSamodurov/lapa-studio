


if(!isMobile()) {
    casesGrid();
    window.onresize = function(event) {
        casesGrid();
    };

}

/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

jQuery.validator.setDefaults({
    errorClass: "invalid",
    validClass: "success",
    errorElement: "div",
    wrapper: "span",
    rules: {
        name: {
            required: true
        },
        phone: {
            required: true,
            minlength: 10
        },
        email: {
            required: true,
            email:true
        },
        personalAgreement: {
            required: true
        }
    },
    messages: {
        name: {
            required: "Это поле обязательно для заполнения"
        },
        phone: {
            required: 'Это поле обязательно для заполнения',
            minlength: 'Телефон минимум 10 символов'
        },
        email: {
            required: "Это поле обязательно для заполнения",
            email: "Введите правильный email"
        },
        personalAgreement: {
            required: "Согласие на обработку персональных данных обязательно"
        }
    }

});


/* --------------------------
        ADAPTIVE SLIDES
 --------------------------- */


function initBulletSlider() {
    if(isMobile()) {
        $('.bullets__wrapper').slick({
            arrows: false,
            infinite: false,
            dots: true,
            adaptiveHeight:true,
            fade: true,
            rows: 4,
            slidesToScroll: 1,
            slidesToShow: 1,
            centerMode: true
        });
    }
}


$(function () {
    if(isMobile()) {

        $('.cases__wrapper').slick({
            arrows: false,
            infinite: false,
            dots: true
        });

        initBulletSlider();

        $('.tarifs__wrapper').slick({
            arrows: false,
            infinite: true,
            //adaptiveHeight:true,
            //slidesToScroll: 1,
            //slidesToShow: 1,
            //centerMode: true
            //variableWidth: true,
            //fade: true
        });
        $('.tarifs__hidden-nav .btn_outline').click(() => {
            $('.tarifs__wrapper').slick('slickNext');
        })
    }
});




/* --------------------------
        ANIMATIONS
 --------------------------- */
$(function(){
    var topScreenAnimation = new TimelineMax();
    topScreenAnimation
        .fromTo('.home .title', 1, {opacity:0, y: 40}, {opacity:1, y: 0}, 0.3)
        .fromTo('.home .down-btn',1,{opacity:0, y: -20},{opacity:1, y: 0}, "-=0.1");



    var casesPoint = $('.cases').waypoint(function(direction){
            TweenMax.staggerTo(".cases__item", 1, {opacity: 1});
            TweenMax.staggerFrom(".cases__item", 1, {y: 1, ease: Elastic.easeOut.config(1, 1)});

            if($(window).width() >= 736) {
                rippleGenerator(".ripples",100, 20, function(){
                    TweenMax.staggerFrom(".ripple-item", 1,
                        {opacity:0, width: "-=50", height: "-=50", ease: Elastic.easeOut.config(1, 0.5)}, 0.1);
                });
            }

            this.destroy()
        }, {
        offset: '30%'
    });


    var bulletsAnimation = $('.bullets').waypoint(function(direction){
        TweenMax.staggerTo(".bullets__item", 1, {opacity: 1}, 0.1);
        TweenMax.staggerFrom(".bullets__item", 1, {y: 40, ease: Elastic.easeOut.config(1, 1)});
        this.destroy()
    }, {
        offset: '30%'
    });




});









$(function(){

    $(window).resize(function(){
        casesGrid();
        initBulletSlider();
    });


    $("[data-type=ajax]").fancybox({
        autoFocus : false,
        backFocus : false,
        trapFocus : false
    });


    lpcFaq.init({
        speed: 250
    });

    $('#down-btn-1').click((e) => {
        e.preventDefault();
        $.scrollTo('.cases', 300);
    });
    $('#down-btn-2').click((e) => {
        e.preventDefault();
        $.scrollTo('.bullets', 300, {
            offset: -60
        });
    });
    $('#down-btn-3').click((e) => {
        e.preventDefault();
        $.scrollTo('.tarifs', 300, {
            offset: -65
        });
    });


    $(window).scroll(() => {
        let homeScreenHeight = $('.home').outerHeight();
        let tmp = $(window).scrollTop();
        if(tmp >= homeScreenHeight) {
            $('.header').addClass('header_visible');
        } else {
            $('.header').removeClass('header_visible');
        }
    });



    $('input[name=phone]').mask('0 (000) 000-00-00');
    // Обработка блока "Политики безопасности"
    $(".lpc-policy__detail").click(function(e) {
        e.preventDefault();
        $(this).closest('.lpc-policy').find('.lpc-policy__hidden').slideToggle(240);
    });



    // Показываем модальное окно с формой захвата
    $('.showModal').click(() => {
        $.fancybox.open({
            src: "#popup_form",
            type: "inline",
            autoFocus : false,
            backFocus : false,
            trapFocus : false,
            modal : false,
            animationEffect : "zoom",
            touch : false
        })
    });

});



/*
 * Валидация формы и отправка данных
 * на сервер при успешной валидации
 */

$('form').submit(function(){
    if($(this).valid()) {
        var form = $(this);
        var link = $(form).attr('action'),
            data = $(form).serialize(),
            name = $(form).find('input[name=name]').val(),
            email = $(form).find('input[name=email]').val(),
            phone = $(form).find('input[name=phone]').val(),
            btnText = $(form).find('button[type=submit]').text();

        $.ajax({
            url: link,
            data: data,
            method: "POST",
            beforeSend: () => {
                $(form)
                    .find('button[type=submit]')
                    .prop("disabled", true)
                    .html('Отправляем...');
            }
        }).done(() => {
            sendTargets();
            $(form)[0].reset();
            $.fancybox.open({
                src: "#ty_popup",
                type: "inline"
            })

        }).fail(() => {
            alert("fail");

        }).always(() => {
            $(form)
                .find('button[type=submit]')
                .prop("disabled", false)
                .text(btnText);
        });
    }
    return false;
});




/**
 * Отправка целей в метрики
 * @returns {boolean}
 */
function sendTargets() {
    try {
        //ga('send', 'event', 'target_name', 'Send_Form');
        //yaCounter27354854.reachGoal('target_name');
    } catch (e) {
        console.log(e);
    }
    return false;
}



var lpcFaq = {
    init: function (options) {
        var speed = options.speed || 250;
        $('.faq__item').click(function(){
            if($(this).hasClass('faq__item_active')) {
                $(this).removeClass('faq__item_active');
                $(this).find('.faq__item-content').slideUp(speed);
            } else {
                $(this).siblings().removeClass('faq__item_active');
                $(this).siblings().find('.faq__item-content').slideUp(speed);
                $(this).addClass('faq__item_active');
                $(this).find('.faq__item-content').slideDown(speed);
            }
        });
    }
};


function isMobile() {
    return $(window).width() <= 736;
}



function rippleGenerator(elem, count, delta, callback) {
    var elem = $(elem),
        initRadius = 40,
        out = "";

    for(var i=0; i <= count; i++) {
        delta += 2;
        initRadius += delta;
        out += `<div class="ripple-item" style="width: ${initRadius}px; height: ${initRadius}px;"></div>`;
    }
    elem.html(out);
    try{
        callback();
    } catch(e){
        console.log(e)
    }
}




function casesGrid(){
    var cw = $(".cases__wrapper").outerWidth()/5,
        cwOffset = 35,
        tmp = 0,
        items = $(".cases__item");

    //alert(cw);
    if($(window).width() > 1940) {
        cwOffset = 120;
    }


    items.css("width", cw).css("height", cw);
    items.each((index, item) => {
        tmp += cwOffset;
        if(index < 4) {
            $(item).css("-webkit-transform", `translateY(-${tmp}px)`);
            $(item).css("transform", `translateY(-${tmp}px)`);
        }
        if(index == 4) {
            $(item).css("-webkit-transform", `translateY(-${tmp-100}px)`);
            $(item).css("transform", `translateY(-${tmp-100}px)`);
        }
        if(index == 5) {
            $(item).css("-webkit-transform", `translate(${cw*3}px, -${cwOffset*4}px)`);
        }
        if(($(window).width() > 1940) && (index == 4)) {
            $(item).css("-webkit-transform", `translateY(-${cwOffset*3}px)`);
        }
    });


    // For Sanek PC
    if($(window).width() > 1340 && $(window).width() < 1380) {
        items.each((index, item) => {
            if(index == 3) {
                $(item).css("-webkit-transform", `translateY(-${cwOffset+50}px)`);
                $(item).css("transform", `translateY(-${tmp}px)`);
            }
            if(index == 5) {
                $(item).css("-webkit-transform", `translate(${cw*3}px, -${cwOffset+175}px)`);
            }
        });
    }
}
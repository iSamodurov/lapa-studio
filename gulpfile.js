'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin'),
    svgo = require('gulp-svgo'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    merge = require('merge-stream'),
    plumber = require('gulp-plumber'),
    newer = require('gulp-newer'),
    pug = require('gulp-pug'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    importPostCss = require("import-postcss"),
    htmlmin = require('gulp-htmlmin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    babel = require('gulp-babel'),
    mqpacker = require('css-mqpacker'),
    reload = browserSync.reload;


var path = {
    production: {
        html: 'production/',
        js: 'production/js/',
        css: 'production/css/',
        img: 'production/img/',
        php: 'production/php/',
        fonts: 'production/fonts/'
    },
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        php: 'build/php/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/pages/*.pug',
        js: ['src/js/**/*.js','!src/js/main.js'],
        style: 'src/style/main.scss',
        img: 'src/img/**/*.*',
        php: 'src/php/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.pug',
        js: 'src/js/main.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        php: 'src/php/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    vendorLibs: [
        "node_modules/sourcebuster/dist/sourcebuster.min.js",
        "node_modules/jquery-validation/dist/jquery.validate.js",
        "node_modules/ouibounce/build/ouibounce.min.js",
        "src/js/modernizr-custom.js",
        "src/js/jquery.fancybox.min.js"
    ],
    clean: './build'
};



var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "frontendKit"
};


// ---- SYSTEM TASKS ---------
gulp.task('webserver', function () {
    browserSync(config);
});
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});



gulp.task('separateScripts', function () {
    gulp.src(path.vendorLibs)
        .pipe(plumber())
        .pipe(gulp.dest(path.build.js))
});




gulp.task('html:build', function () {
    gulp.start('html:buildModals');
    gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});
gulp.task('html:buildModals', function () {
    gulp.src('./src/pages/modals/*.pug')
        .pipe(plumber())
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest(path.build.html + 'modals/'))
        .pipe(reload({stream: true}));
});




// ------- BUILD JAVASCRIPT -------------
gulp.task('js:build', function () {
    gulp.src('src/js/main.js')
        .pipe(plumber())
        .pipe(babel({
            presets: ['env']
         }))
        //.pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});







// ---------- BUILD STYLES --------------
gulp.task('style:build', function () {
    var plugins = [
        autoprefixer({browsers: ['last 3 versions']}),
        importPostCss(),
        mqpacker()
        //cssnano()
    ];
    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(plugins))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});


// ----------- IMAGES BUILD ----------------

gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin([
            imageminJpegRecompress()
        ], {
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            interlaced: true
        }))
        .pipe(svgo())
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});


gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});



gulp.task('php:build', function() {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
});




gulp.task('build', [
    'separateScripts',
    'image:build',
    'html:build',
    'js:build',
    'style:build',
    'php:build',
    'fonts:build'
]);



gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) { gulp.start('html:build'); });
    watch([path.watch.style], function(event, cb) { gulp.start('style:build'); });
    watch([path.watch.js], function(event, cb) { gulp.start('js:build'); });
    watch([path.watch.img], function(event, cb) { gulp.start('image:build'); });
    watch([path.watch.fonts], function(event, cb) { gulp.start('fonts:build'); });
    watch([path.watch.php], function(event, cb) { gulp.start('php:build'); });
});



gulp.task('default', ['build', 'webserver', 'watch']);





